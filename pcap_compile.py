import os
import argparse

def main():
    parser = argparse.ArgumentParser(description="build and run caperdump")
    parser.add_argument("-p", "--pcap")
    args = parser.parse_args()

    os.system(f"../caper/caper.byte -BPF_optimized -max_rec 4 -q -e '{args.pcap}' > bpf.labelled")
    os.system("../c2/_build/coton.native --convert_labelled_to_symbolic --absolute_jump_offsets bpf.labelled > bpf.symbolic")
    os.system("../c2/_build/coton.native --convert_symbolic_to_numeric --absolute_jump_offsets bpf.symbolic")

if __name__ == "__main__":
    main()